package TML.test.myFruitsDiary.ViewModel;

import java.util.ArrayList;

import TML.test.myFruitsDiary.Classes.Entries;
import TML.test.myFruitsDiary.Classes.Fruits;
import TML.test.myFruitsDiary.Repositories.EntriesRepository;
import TML.test.myFruitsDiary.Repositories.FruitsRepository;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class EntriesViewModel extends ViewModel {

    private LiveData<ArrayList<Entries>> entriesMutableLiveData;

    public void initEntries() {
        entriesMutableLiveData = EntriesRepository.getEntries();
    }
    public LiveData<ArrayList<Entries>> getEntries() {
        return entriesMutableLiveData;
    }
}

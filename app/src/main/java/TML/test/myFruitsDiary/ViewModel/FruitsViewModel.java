package TML.test.myFruitsDiary.ViewModel;

import java.util.ArrayList;

import TML.test.myFruitsDiary.Classes.Fruits;
import TML.test.myFruitsDiary.Repositories.FruitsRepository;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class FruitsViewModel extends ViewModel {

    private MutableLiveData<ArrayList<Fruits>> fruitsMutableLiveData;

    public void initFruitsData() {
        fruitsMutableLiveData = FruitsRepository.getFruits();
    }
    public LiveData<ArrayList<Fruits>> getFruits() {
        return fruitsMutableLiveData;
    }
}

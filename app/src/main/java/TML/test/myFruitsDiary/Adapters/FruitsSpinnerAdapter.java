package TML.test.myFruitsDiary.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import TML.test.myFruitsDiary.Classes.Fruits;
import TML.test.myFruitsDiary.R;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class FruitsSpinnerAdapter extends ArrayAdapter<Fruits> {

    private final String urlLink = "https://fruitdiary.test.themobilelife.com/";

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    public FruitsSpinnerAdapter(@NonNull Context context, ArrayList<Fruits> fruits) {
        super(context, 0, fruits);
    }

    public View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_fruit_spinner_row, parent, false);
        }

        ImageView ivFruits = convertView.findViewById(R.id.iv_fruit_spinner);
        TextView tvVitamin = convertView.findViewById(R.id.tv_fruit_vitamin_spinner);
        TextView tvFruitType = convertView.findViewById(R.id.tv_fruit_type_spinner);

        Fruits fruits = getItem(position);
        if (fruits != null) {
            Picasso.get().load(urlLink + fruits.getImage()).fit().into(ivFruits);
            tvVitamin.setText(getContext().getString(R.string.totalVitWithColon) + fruits.getVitamins());
            tvFruitType.setText(getContext().getString(R.string.fruitTypeWithColon) + fruits.getType());
        }
        return convertView;
    }
}

package TML.test.myFruitsDiary.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import TML.test.myFruitsDiary.Classes.Entries;
import TML.test.myFruitsDiary.Fragments.myDiaryFragmentDirections;
import TML.test.myFruitsDiary.R;
import TML.test.myFruitsDiary.ViewHolders.DiaryViewHolder;
import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

public class DiaryRecyclerViewAdapter extends RecyclerView.Adapter<DiaryViewHolder> {

    private Context mContext;
    private ArrayList<Entries> mData;

    public DiaryRecyclerViewAdapter(Context mContext, ArrayList<Entries> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public DiaryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.rv_diary_layout, parent, false);
        return new DiaryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DiaryViewHolder holder, int position) {
        int tempTotalFruitsEaten = 0;

        //Calculate total fruits eaten
        if (mData.get(position).getFruit().size() > 0) {
            for (Entries.Fruit fruit : mData.get(position).getFruit()
            ) {
                tempTotalFruitsEaten += fruit.getAmount();
            }
        }

        holder.itemView.setId(mData.get(position).getId());
        holder.setTvEntryDate(mContext.getString(R.string.dateWithColon) + mData.get(position).getDate());
        holder.setTvNoOfFruits(mContext.getString(R.string.totalFruitsWithColon) + tempTotalFruitsEaten);
        holder.setTvTotalVitamin(mContext.getString(R.string.totalVitWithColon) + mData.get(position).getTotalVitamins());

        //SET onclick listener for item row
        holder.itemView.setOnClickListener(view -> {
            myDiaryFragmentDirections.ActionMyDiaryFragmentToUpdateEntryFragment action =
                    myDiaryFragmentDirections.actionMyDiaryFragmentToUpdateEntryFragment(mData.get(position));
            Navigation.findNavController(view).navigate(action);
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void updateData(final ArrayList<Entries> entries ) {
        mData = entries;
        notifyDataSetChanged();
    }
}

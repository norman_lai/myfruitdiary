package TML.test.myFruitsDiary.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import TML.test.myFruitsDiary.Classes.Entries;
import TML.test.myFruitsDiary.Classes.Fruits;
import TML.test.myFruitsDiary.R;
import TML.test.myFruitsDiary.ViewHolders.FruitsEntryViewHolder;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FruitsEntryRecyclerViewAdapter extends RecyclerView.Adapter<FruitsEntryViewHolder> {

    private Context mContext;
    private Entries mData;
    private ArrayList<Fruits> fruits;

    public FruitsEntryRecyclerViewAdapter(Context mContext, Entries mData, ArrayList<Fruits> fruits) {
        this.mContext = mContext;
        this.mData = mData;
        this.fruits = fruits;
    }

    @Override
    public FruitsEntryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.rv_fruits_entry_layout, parent, false);
        return new FruitsEntryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FruitsEntryViewHolder holder, int position) {
        //get fruits image
        for (Fruits fruit :
                fruits) {
            if (fruit.getId() == mData.getFruit().get(position).getFruitId()) {
                holder.setIvFruit(fruit.getImage());
                holder.setTvFruitVitamin(mContext.getString(R.string.vitaminWithColon) +fruit.getVitamins());
            }
        }
        holder.setTvFruitType(mContext.getString(R.string.fruitTypeWithColon) + mData.getFruit().get(position).getFruitType());
        holder.setTvFruitAmount(mContext.getString(R.string.fruitAmountWithColon) + mData.getFruit().get(position).getAmount());
        holder.itemView.setId(mData.getFruit().get(position).getFruitId());
    }

    @Override
    public int getItemCount() {
        return mData.getFruit().size();
    }

    public void updateData(Entries entries) {
        mData = entries;
        notifyDataSetChanged();
    }
}

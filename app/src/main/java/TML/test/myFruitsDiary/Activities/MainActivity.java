package TML.test.myFruitsDiary.Activities;

import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import TML.test.myFruitsDiary.Fragments.updateEntryFragment;
import TML.test.myFruitsDiary.R;
import TML.test.myFruitsDiary.Fragments.aboutFragment;
import TML.test.myFruitsDiary.Fragments.myDiaryFragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity implements myDiaryFragment.OnFragmentInteractionListener,
        aboutFragment.OnFragmentInteractionListener, updateEntryFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        BottomNavigationView bottomNav = findViewById(R.id.bottom_nav);
        NavigationUI.setupWithNavController(bottomNav, navController);
    }

}

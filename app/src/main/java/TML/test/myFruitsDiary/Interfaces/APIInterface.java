package TML.test.myFruitsDiary.Interfaces;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import TML.test.myFruitsDiary.Classes.EntryRequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {
    @GET("fruit")
    Call<JsonArray> GetFruits();

    @GET("entries")
    Call<JsonArray> GetEntries();

    @DELETE("entries")
    Call<JsonObject> DeleteAllEntries();

    @DELETE("entry/{entryId}")
    Call<JsonObject> DeleteSpecificEntry(
            @Path(value = "entryId", encoded = true) int entryId
    );

    @POST("entries")
    Call<JsonObject> AddEntry(
            @Body EntryRequestBody body
    );

    @POST("entry/{entryId}/fruit/{fruitId}/")
    Call<JsonObject> AddFruitToEntry(
            @Path(value = "entryId", encoded = true) int entryId,
            @Path(value = "fruitId", encoded = true) int fruitId,
            @Query("amount") int amount
    );
}

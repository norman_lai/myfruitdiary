package TML.test.myFruitsDiary.ViewHolders;

import android.view.View;
import android.widget.TextView;
import TML.test.myFruitsDiary.R;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DiaryViewHolder extends RecyclerView.ViewHolder {
    TextView tvEntryDate, tvNoOfFruits, tvTotalVitamin;

    public DiaryViewHolder(@NonNull View itemView) {
        super(itemView);

        tvEntryDate = itemView.findViewById(R.id.tv_entry_date);
        tvNoOfFruits = itemView.findViewById(R.id.tv_no_of_fruits);
        tvTotalVitamin = itemView.findViewById(R.id.tv_total_vitamin);
    }

    public void setTvEntryDate(String entryDate) {
        this.tvEntryDate.setText(entryDate);
    }

    public void setTvNoOfFruits(String noOfFruits) {
        this.tvNoOfFruits.setText(noOfFruits);
    }

    public void setTvTotalVitamin(String totalVitamin) {
        this.tvTotalVitamin.setText(totalVitamin);
    }
}

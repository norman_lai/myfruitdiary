package TML.test.myFruitsDiary.ViewHolders;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import TML.test.myFruitsDiary.R;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FruitsEntryViewHolder extends RecyclerView.ViewHolder {
    private final String urlLink = "https://fruitdiary.test.themobilelife.com/";

    ImageView ivFruit;
    TextView tvFruitType, tvFruitVitamin, tvFruitAmount;

    public FruitsEntryViewHolder(@NonNull View itemView) {
        super(itemView);
        ivFruit = itemView.findViewById(R.id.iv_fruit);
        tvFruitType = itemView.findViewById(R.id.tv_fruit_type);
        tvFruitVitamin = itemView.findViewById(R.id.tv_fruit_vitamin);
        tvFruitAmount = itemView.findViewById(R.id.tv_fruit_amount);
    }

    public void setIvFruit(String fruitsDrawable) {
        Picasso.get().load(urlLink + fruitsDrawable).fit().into(ivFruit);
    }

    public void setTvFruitType(String fruitType) {
        tvFruitType.setText(fruitType);
    }

    public void setTvFruitVitamin(String fruitVitamin) {
        tvFruitVitamin.setText(fruitVitamin);
    }

    public void setTvFruitAmount(String fruitAmount){
        tvFruitAmount.setText(fruitAmount);
    }
}

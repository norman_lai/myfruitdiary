package TML.test.myFruitsDiary.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import TML.test.myFruitsDiary.Adapters.DiaryRecyclerViewAdapter;
import TML.test.myFruitsDiary.Classes.APIClient;
import TML.test.myFruitsDiary.Classes.Entries;
import TML.test.myFruitsDiary.Classes.EntryRequestBody;
import TML.test.myFruitsDiary.Classes.Fruits;
import TML.test.myFruitsDiary.Interfaces.APIInterface;
import TML.test.myFruitsDiary.R;
import TML.test.myFruitsDiary.ViewHolders.DiaryViewHolder;
import TML.test.myFruitsDiary.ViewModel.EntriesViewModel;
import TML.test.myFruitsDiary.ViewModel.FruitsViewModel;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class myDiaryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //Local initialize
    private RecyclerView mRecyclerView;
    private FloatingActionButton mFloatingActionButton;
    private DiaryRecyclerViewAdapter mRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private Context mContext;
    ArrayList<Entries> entries;
    ArrayList<Fruits> fruitArrayList = new ArrayList<>();
    //ViewModel
    EntriesViewModel entriesViewModel;
    FruitsViewModel fruitsViewModel;

    private OnFragmentInteractionListener mListener;

    public myDiaryFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static myDiaryFragment newInstance(String param1, String param2) {
        myDiaryFragment fragment = new myDiaryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mContext = this.getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_diary, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        //Set Live Data for fruits
        configureFruitsLiveModel();
    }

    //Configure Live Models
    private void configureFruitsLiveModel() {
        fruitsViewModel = ViewModelProviders.of(this).get(FruitsViewModel.class);
        fruitsViewModel.initFruitsData();
        fruitsViewModel.getFruits().observe(this, fruits -> {
            if (fruits != null) {
                fruitArrayList = fruits;
                configureEntriesLiveModel();
            }
        });
    }

    private void configureEntriesLiveModel() {
        entriesViewModel = ViewModelProviders.of(this).get(EntriesViewModel.class);
        entriesViewModel.initEntries();
        entriesViewModel.getEntries().observe(this, entries -> {
            if (entries != null) {
                this.entries = entries;

                //Get Total Vitamins
                getTotalVitamins();

                //Create RecyclerView
                mRecyclerViewAdapter = new DiaryRecyclerViewAdapter(mContext, entries);
                linearLayoutManager = new LinearLayoutManager(mContext);
                mRecyclerView.setLayoutManager(linearLayoutManager);
                mRecyclerView.setAdapter(mRecyclerViewAdapter);
                //swipe to delete
                initSwipe();
            }
        });
    }
    //#Configure Ends


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //find view
        mRecyclerView = view.findViewById(R.id.rv_diary);
        mFloatingActionButton = view.findViewById(R.id.fab_add);

        //onClick listener
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogForAddingEntry();
            }
        });
    }

    //Extra Functionality
    private void showDialogForAddingEntry() {
        //inflate custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.custom_dialog_for_add_entry_layout, null);
        AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        //find view
        Button okButton = customLayout.findViewById(R.id.btn_ok_add_dialog);
        Button cancelButton = customLayout.findViewById(R.id.btn_cancel_add_dialog);
        DatePicker datePicker = customLayout.findViewById(R.id.dp_entry);

        builder.setView(customLayout);

        AlertDialog dialog = builder.create();

        dialog.show();

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call api to add entry
                String paramDate = convertDMY(datePicker);
                //Format paramDate
                EntryRequestBody entryRequestBody = new EntryRequestBody(paramDate);
                addEntry(entryRequestBody, dialog);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void addEntry(EntryRequestBody entryRequestBody, AlertDialog dialog) {
        //Call API for entries
        APIInterface apiInterface = APIClient.getAPIClient().create(APIInterface.class);
        Call<JsonObject> call = apiInterface.AddEntry(entryRequestBody);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    Toast.makeText(mContext, mContext.getString(R.string.add_successfully), Toast.LENGTH_LONG).show();
                    entriesViewModel.initEntries();
                    dialog.dismiss();
                } else {
                    //if failed in case 404
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(mContext, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Connection fail or others
                Toast.makeText(mContext, mContext.getString(R.string.something_wrong), Toast.LENGTH_LONG).show();
            }
        });
    }

    private String convertDMY(DatePicker datePicker) {
        int year = datePicker.getYear();
        int month = datePicker.getMonth();
        int day = datePicker.getDayOfMonth();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return format.format(calendar.getTime());
    }

    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.RIGHT)
                    popOutConfirmationDialog(viewHolder.itemView.getId());
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                DiaryViewHolder viewHolder1 = (DiaryViewHolder) viewHolder;
                int position = viewHolder1.getAdapterPosition();
                Entries item = entries.get(position);
                if (item == null) return;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    Paint p = new Paint();
                    Paint textPaint = new Paint();
                    textPaint.setColor(Color.WHITE);
                    textPaint.setTextSize(20);

                    if (dX > 0) {
                        p.setColor(Color.parseColor("#000000"));
                        RectF background = new RectF((float) itemView.getLeft() + dX, (float) itemView.getTop(), (float) itemView.getLeft(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        Bitmap icon = drawableToBitmap(getResources().getDrawable(R.drawable.ic_remove));
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void popOutConfirmationDialog(int id) {
        AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);

        builder.setPositiveButton(R.string.general_ok, (dialogInterface, i) -> {
            deleteEntry(dialogInterface, id);
        })
                .setNegativeButton(R.string.general_cancel, (dialogInterface, i) -> {
                    mRecyclerViewAdapter.notifyDataSetChanged();
                    dialogInterface.dismiss();
                })
                .setTitle(R.string.delete_confirmation);


        AlertDialog dialog = builder.create();

        dialog.show();

    }

    private void deleteEntry(DialogInterface dialogInterface, int id) {
        APIInterface apiInterface = APIClient.getAPIClient().create(APIInterface.class);
        Call<JsonObject> call = apiInterface.DeleteSpecificEntry(id);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    Toast.makeText(mContext, mContext.getString(R.string.delete_successfully), Toast.LENGTH_LONG).show();
                    dialogInterface.dismiss();
                    entriesViewModel.initEntries();
                } else {
                    //if failed in case 404
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(mContext, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Connection fail or others
                Toast.makeText(mContext, mContext.getString(R.string.something_wrong), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getTotalVitamins() {
        //WHY DO SOMETHING LIKE THIS WHEN YOU CAN CALCULATE AT BACK END AND RETURN BACK IN 1 API....
        int tempTotalVitamins = 0;
        for (Entries entry : entries
        ) {
            if (entry.getFruit() != null || entry.getFruit().size() != 0) {
                //Loop inner fruit to check for fruits
                for (Entries.Fruit entriesFruit : entry.getFruit()
                ) {
                    //Compare if same id
                    if (fruitArrayList.size() > 0) {
                        for (Fruits fruit : fruitArrayList
                        ) {
                            if (fruit.getId() == entriesFruit.getFruitId()) {
                                //total amount of entry fruits * fruit vitamins
                                tempTotalVitamins += fruit.getVitamins() * entriesFruit.getAmount();
                            }
                        }

                    }
                }
            }
            entry.setTotalVitamins(tempTotalVitamins);
            tempTotalVitamins = 0;
        }
    }
    // #Ends functionality

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }
}

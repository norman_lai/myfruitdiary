package TML.test.myFruitsDiary.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import TML.test.myFruitsDiary.Adapters.FruitsEntryRecyclerViewAdapter;
import TML.test.myFruitsDiary.Adapters.FruitsSpinnerAdapter;
import TML.test.myFruitsDiary.Classes.APIClient;
import TML.test.myFruitsDiary.Classes.Entries;
import TML.test.myFruitsDiary.Classes.EntryRequestBody;
import TML.test.myFruitsDiary.Classes.Fruits;
import TML.test.myFruitsDiary.Interfaces.APIInterface;
import TML.test.myFruitsDiary.ViewHolders.DiaryViewHolder;
import TML.test.myFruitsDiary.ViewHolders.FruitsEntryViewHolder;
import TML.test.myFruitsDiary.ViewModel.FruitsViewModel;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import TML.test.myFruitsDiary.R;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class updateEntryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context mContext;
    private LinearLayoutManager linearLayoutManager;
    private FruitsViewModel fruitsViewModel;
    private ArrayList<Fruits> fruitsArrayList;
    private Entries entries;
    private RecyclerView mRecyclerView;
    private FruitsEntryRecyclerViewAdapter mRecyclerViewAdapter;
    private FruitsSpinnerAdapter mSpinnerAdapter;
    Fruits fruitsSelected;

    private OnFragmentInteractionListener mListener;

    public updateEntryFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static updateEntryFragment newInstance(String param1, String param2) {
        updateEntryFragment fragment = new updateEntryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        //setup Viewmodel
        fruitsViewModel = ViewModelProviders.of(requireActivity()).get(FruitsViewModel.class);
        mContext = this.getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_update_entry, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        entries = updateEntryFragmentArgs.fromBundle(getArguments()).getEntries();
        //find view by id
        TextView tvTitle = view.findViewById(R.id.tv_entry_date_title);
        mRecyclerView = view.findViewById(R.id.rv_entry_fruits);
        Button addNewEntryBtn = view.findViewById(R.id.btn_add_new_fruit);

        addNewEntryBtn.setOnClickListener(view1 -> addFruit());

        tvTitle.setText(mContext.getString(R.string.dateWithColon) + entries.getDate());
        //Configure Viewmodel
        configureFruitsLiveModel();
    }

    private void configureFruitsLiveModel() {
        fruitsViewModel = ViewModelProviders.of(this).get(FruitsViewModel.class);
        fruitsViewModel.initFruitsData();
        fruitsViewModel.getFruits().observe(this, fruits -> {
            if (fruits != null) {
                fruitsArrayList = fruits;
                //Set Title
                //Setup recyclerview
                mRecyclerViewAdapter = new FruitsEntryRecyclerViewAdapter(mContext, entries, fruitsArrayList);
                linearLayoutManager = new LinearLayoutManager(mContext);
                mRecyclerView.setLayoutManager(linearLayoutManager);
                mRecyclerView.setAdapter(mRecyclerViewAdapter);
                initSwipe();
            }
        });
    }

    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.RIGHT) {
                    popOutConfirmationDialog(viewHolder.itemView.getId());
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                FruitsEntryViewHolder viewHolder1 = (FruitsEntryViewHolder) viewHolder;
                int position = viewHolder1.getAdapterPosition();
                Entries.Fruit item = entries.getFruit().get(position);
                if (item == null) return;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    Paint p = new Paint();
                    Paint textPaint = new Paint();
                    textPaint.setColor(Color.WHITE);
                    textPaint.setTextSize(20);

                    if (dX > 0) {
                        p.setColor(Color.parseColor("#990000"));
                        RectF background = new RectF((float) itemView.getLeft() + dX, (float) itemView.getTop(), (float) itemView.getLeft(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        Bitmap icon = drawableToBitmap(getResources().getDrawable(R.drawable.ic_edit));
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private void popOutConfirmationDialog(int fruitId) {
        AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);

        builder.setPositiveButton(R.string.general_ok, (dialogInterface, i) -> {
            dialogInterface.dismiss();
            editFruit(fruitId);
        })
                .setNegativeButton(R.string.general_cancel, (dialogInterface, i) -> {
                    mRecyclerViewAdapter.notifyDataSetChanged();
                    dialogInterface.dismiss();
                })
                .setTitle(R.string.edit_confirmation);

        AlertDialog dialog = builder.create();

        dialog.show();

    }

    private void editFruit(int fruitId) {
        AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        int amount = 0;
        for (Entries.Fruit fruit :
                entries.getFruit()) {
            if (fruit.getFruitId() == fruitId)
                amount = fruit.getAmount();
        }
        final EditText input = new EditText(mContext);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setText(String.valueOf(amount));

        builder.setPositiveButton(R.string.general_ok, (dialogInterface, i) -> {
            if(fruitsSelected == null){
                fruitsSelected = new Fruits(fruitId, null, 0, null);
            }
            fruitsSelected.setId(fruitId);
            editEntry(input.getText().toString());
            dialogInterface.dismiss();
        })
                .setNegativeButton(R.string.general_cancel, (dialogInterface, i) -> {
                    mRecyclerViewAdapter.notifyDataSetChanged();
                    dialogInterface.dismiss();
                })
                .setTitle(R.string.insert_amount).setView(input);

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private void addFruit() {
        final View customLayout = getLayoutInflater().inflate(R.layout.custom_dialog_for_edit_fruit_layout, null);
        AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        //find view
        Spinner fruitSpinner = customLayout.findViewById(R.id.fruit_spinner);
        Button okButton = customLayout.findViewById(R.id.btn_ok_add_dialog_edit);
        Button cancelButton = customLayout.findViewById(R.id.btn_cancel_add_dialog_edit);
        EditText etFruitAmount = customLayout.findViewById(R.id.et_fruit_amount_edit);

        etFruitAmount.setText("0");

        //init Spinner
        mSpinnerAdapter = new FruitsSpinnerAdapter(mContext, fruitsArrayList);
        fruitSpinner.setAdapter(mSpinnerAdapter);
        fruitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                fruitsSelected = (Fruits) adapterView.getItemAtPosition(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        builder.setView(customLayout);

        AlertDialog dialog = builder.create();

        dialog.show();

        okButton.setOnClickListener(view -> {
            //call api to edit entry
            editEntry(etFruitAmount.getText().toString());
            dialog.dismiss();
        });

        cancelButton.setOnClickListener(view -> {
                    dialog.dismiss();
                    mRecyclerViewAdapter.notifyDataSetChanged();
                }
        );
    }

    private void editEntry(String enteredAmount) {
        //set fruits into seperate variable
        int entryId = entries.getId();
        int fruitsSelectedId = fruitsSelected.getId();
        int fruitAmount = Integer.parseInt(enteredAmount);

        APIInterface apiInterface = APIClient.getAPIClient().create(APIInterface.class);
        Call<JsonObject> call = apiInterface.AddFruitToEntry(entryId, fruitsSelectedId, fruitAmount);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    Toast.makeText(mContext, mContext.getString(R.string.edit_successfully), Toast.LENGTH_LONG).show();
                    initEntriesData(fruitsSelectedId, fruitsSelected.getType(),fruitAmount);
                } else {
                    //if failed in case eg :404
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(mContext, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Connection fail or others
                Toast.makeText(mContext, mContext.getString(R.string.something_wrong), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initEntriesData(int fruitsSelectedId, String fruitType, int fruitAmount) {
        boolean edit = false;
        for (Entries.Fruit fruit :
                entries.getFruit()) {
            if (fruit.getFruitId() == fruitsSelectedId) {
                edit = true;
                fruit.setAmount(fruitAmount);
                mRecyclerViewAdapter.updateData(entries);
            }
        }
        if(!edit){
            entries.getFruit().add(new Entries.Fruit(fruitsSelectedId, fruitType, fruitAmount));
            mRecyclerViewAdapter.updateData(entries);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }
}

package TML.test.myFruitsDiary.Classes;

import androidx.lifecycle.MutableLiveData;

public class Fruits
{
    int id;
    String type, image;
    int vitamins;

    public Fruits(int id, String type, int vitamins, String image) {
        this.id = id;
        this.type = type;
        this.vitamins = vitamins;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public int getVitamins() {
        return vitamins;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }
}

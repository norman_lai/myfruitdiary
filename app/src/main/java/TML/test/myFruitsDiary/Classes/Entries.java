package TML.test.myFruitsDiary.Classes;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Entries implements Parcelable {
    String date;
    int id, totalVitamins;
    ArrayList<Fruit> fruit;

    public Entries(int id, String date, ArrayList<Fruit> fruit, int totalVitamins) {
        this.id = id;
        this.date = date;
        this.fruit = fruit;
        this.totalVitamins = totalVitamins;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public ArrayList<Fruit> getFruit() {
        return fruit;
    }

    public int getTotalVitamins() {
        return totalVitamins;
    }

    public void setTotalVitamins(int totalVitamins) {
        this.totalVitamins = totalVitamins;
    }

    public static class Fruit {
        int fruitId;
        String fruitType;
        int amount;

        public Fruit(int fruitId, String fruitType, int amount) {
            this.fruitId = fruitId;
            this.fruitType = fruitType;
            this.amount = amount;
        }

        public int getFruitId() {
            return fruitId;
        }

        public String getFruitType() {
            return fruitType;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }
    }

    protected Entries(Parcel in) {
        id = in.readInt();
        this.date = in.readString();
        this.fruit = in.readArrayList(fruit.getClass().getClassLoader());
        this.totalVitamins = in.readInt();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(date);
        dest.writeList(fruit);
        dest.writeInt(totalVitamins);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Entries> CREATOR = new Creator<Entries>() {
        @Override
        public Entries createFromParcel(Parcel in) {
            return new Entries(in);
        }

        @Override
        public Entries[] newArray(int size) {
            return new Entries[size];
        }
    };
}



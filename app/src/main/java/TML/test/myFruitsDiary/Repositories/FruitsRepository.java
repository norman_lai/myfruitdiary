package TML.test.myFruitsDiary.Repositories;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import TML.test.myFruitsDiary.Classes.APIClient;
import TML.test.myFruitsDiary.Classes.Fruits;
import TML.test.myFruitsDiary.Interfaces.APIInterface;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FruitsRepository {
    static MutableLiveData<ArrayList<Fruits>> fruitsMutableLiveData = new MutableLiveData<>();

    public static MutableLiveData<ArrayList<Fruits>> getFruits() {
        APIInterface apiInterface = APIClient.getAPIClient().create(APIInterface.class);
        Call<JsonArray> call = apiInterface.GetFruits();
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if (response.code() == 200) {
                    Type type = new TypeToken<ArrayList<Fruits>>() {
                    }.getType();
                    ArrayList<Fruits> fruits = new ArrayList<>();
                    fruits = new Gson().fromJson(response.body().getAsJsonArray(), type);
                    fruitsMutableLiveData.postValue(fruits);

                } else {
                    //if failed in case 404
                    fruitsMutableLiveData = new MutableLiveData<ArrayList<Fruits>>();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                //Connection fail or others
                fruitsMutableLiveData = new MutableLiveData<ArrayList<Fruits>>();
            }
        });

        return fruitsMutableLiveData;
    }
}


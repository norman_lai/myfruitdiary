package TML.test.myFruitsDiary.Repositories;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import TML.test.myFruitsDiary.Adapters.DiaryRecyclerViewAdapter;
import TML.test.myFruitsDiary.Classes.APIClient;
import TML.test.myFruitsDiary.Classes.Entries;
import TML.test.myFruitsDiary.Classes.Fruits;
import TML.test.myFruitsDiary.Interfaces.APIInterface;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EntriesRepository {
    static MutableLiveData<ArrayList<Entries>> entriesMutableLiveData = new MutableLiveData<>();

    public static MutableLiveData<ArrayList<Entries>> getEntries() {
        APIInterface apiInterface = APIClient.getAPIClient().create(APIInterface.class);
        Call<JsonArray> call = apiInterface.GetEntries();
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if (response.code() == 200) {
                    Type type = new TypeToken<ArrayList<Entries>>() {
                    }.getType();
                    ArrayList<Entries> entries = new ArrayList<>();
                    entries = new Gson().fromJson(response.body().getAsJsonArray(), type);
                    entriesMutableLiveData.postValue(entries);
                } else {
                    //if failed in case 404
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                //Connection fail or others
                entriesMutableLiveData = new MutableLiveData<>();
            }
        });

        return entriesMutableLiveData;
    }
}

